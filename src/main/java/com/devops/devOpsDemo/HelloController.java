package com.devops.devOpsDemo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index() {
		return "Hello this is the Spring app2. I'm launched from docker with Jenkins !";
	}

}
